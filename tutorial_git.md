# "Tutorial" de Git


## Introdução

Você já deve ter escrito algum trabalho que precisou fazer alterações e, para garantir que nada fosse perdido, ficou salvando várias cópias "versão sem introdução", "introdução xyz", "pós revisão fulano" e coisas do gênero, certo? Pois bem, se eu te contar que existe uma ferramenta que auxilia nessa gestão, você fica feliz? Existe! Chama-se Git (na realidade, Git é apenas uma possibilidade, exisem diversos gerenciadores de versão).

O Git é então um gerenciador de versão bastante conhecido pela comunidade de desenvolvedores mundo afora. Ele foi criado pelo Linus Torvalds (Linus, o Torvalds) desenvolvedor do Linux exatamente com o propósito de versionar o código do _kernel_. Bom, chega de historinha e vamos lá!


## Funcionalidades

### 1) `init`

O Git é este gerenciador de versões muito bom, mas ele depende dos nossos comandos. Vamos supor que já estamos no nosso diretório de trabalho, onde estão (ou estarão) nossos arquivos. Se for um diretório que não foi você que criou, há chances de já haver o Git "rodando" nele. Faça o teste:

`git status`

Caso a mensagem **não** tenha uma cara como essa, então você pode seguir pro passo 2, porque já tem um Git "rodando" aqui:

```plain
fatal: Not a git repository (or any parent up to mount point /)
Stopping at filesystem boundary (GIT_DISCOVERY_ACROSS_FILESYSTEM not set).
```

> Essa é a mensagem de erro que o Git dá quando você chama sem que ele tenha sido iniciado.


Falhou porque não há uma instância do git, então vamos dar início ao controle! Rode:

`git init`

Isso diz ao Git que você quer que ele gerencie as versões de tudo que está no diretório atual (e em todos os diretórios filho também). Ele vai dizer que iniciou um repositório local:

```
Initialized empty Git repository in /onde/voce/esta/.git/
```

Beleza, agora o Git tá olhando nossa pasta.


Antes de seguirmos para o Ciclo Básico, vamos nos apresentar ao Git. Mais tarde, essas informações nossas serão úteis. Bora:

`git config --global user.name "João Daniel"`

`git config --global user.email "jotaf.daniel@gmail.com"`

Com isso, eu disse que globalmente ele pode me identificar por "João Daniel - <jotaf.daniel@gmail.com>". Por que fizemos isso? Daqui a pouco, veremos como oficializar mudanças feitas no repositório e, para isso, o Git precisa saber quem as realizou. Agora que temos essas informações configuradas, o Git sabe quem eu sou e vai poder vincular as minhas alterações a mim corretamente.


### 2) Ciclo básico

Se você estava num diretório vazio que criou apenas para esse "tutorial", vai notar que agora há apenas um diretório oculto criado pelo Git - é o .git
Este diretório é onde o Git armazena todas as informações necessárias para manter nosso repositório, inicialmente você não precisa se preocupar com nada do que está aí dentro.

Vamos escrever um arquivo nosso e ver como o Git controla? Crie um novo arquivo de texto puro "readme.txt" e escreva (ou cole) o seguinte conteúdo: (texto original da página do [git na wikipedia](https://pt.wikipedia.org/wiki/Git))

```plain
Git pronunciado [git] (ou pronunciado [dit] em inglês britânico) é um sistema de
controle de versão distribuído e um sistema de gerenciamento de código fonte, com
ênfase em velocidade. O Git foi inicialmente projetado e desenvolvido por Linus
Torvalds para o desenvolvimento do kernel Linux, mas foi adotado por muitos outros
projetos.
```

Salve o arquivo e vá para o terminal. Fizemos uma alteração no estado do repositório, certo? Será que o Git notou? Perguntemos a ele:

`git status`

Ele vai dizer que notou algo diferente, que há um arquivo não rastreado:

```
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	readme.txt

nothing added to commit but untracked files present (use "git add" to track)
```

Exatamente o que fizemos! Nosso arquivo novo, criado do zero, e o Git notou que não estava lá. Aprendemos a ver o estado atual do repositório com o `status`.

Mas o que é esse "untracked"? O git foi iniciado no diretório local mas não é tão automático assim, a ponto de podermos sair escrevendo qualquer coisa a qualquer momento e ele simplesmente controla tudo. Você precisa pedir pro git controlar os arquivos que deseja. Parece chato no começo, mas mais pra frente veremos que é bem esperto isso.

Vamos pedir pra ele controlar nosso "readme.txt" com o seguinte:

`git add readme.txt`

Faça o seguinte: rode um `git status` e veja que agora as coisas estão diferentes de como estavam na primeira vez.

```
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   readme.txt
```

Antes nosso arquivo estava como "untracked", agora está anotado do lado dele um "new file". E note mais uma coisa: dentro dos parêntesis, há uma mensagem informando como fazer para "unstage" (algo como "desregistrar"). Pois bem, quando dissemos `git add readme.txt`, o git "mandou" nosso arquivo para uma área de registro. Ainda não dissemos para o Git "beleza, considere o estado atual como uma versão válida", não nos _comprometemos_ com as mudanças feitas. Aqui ainda é fácil mudar mais coisas, voltar atrás, enfim... apenas dissemos para o git ficar ligeiro no arquivo.

Mas, por enquanto, nossas alterações podem ficar por aqui, vamos nos _comprometer_ com elas agora. Vamos dizer ao Git que fechamos o compromisso com essas alterações e vamos deixar uma mensagem identificando o que fizemos:

`git commit -m "cria arquivo readme"`

Note: estamos nos comprometendo (`commit`) e passando como mensagem (`-m`) algo que identifique o trabalho (`"cria arquivo readme"`).

Ele vai dizer algumas coisas para você, algo do tipo:

```
[master (root-commit) 58b365c] cria arquivo readme
 1 file changed, 5 insertions(+)
 create mode 100644 readme.txt
```

Nessa mensagem, vamos olhar por partes:
- "master" é nosso ramo de desenvolvimento (mais tarde falaremos de ramos com mais calma)
- "58b365c" é um hash identificador do _commit_, para quando precisarmos fazer referência a ele
- "5 insertions(+)" mas só tivemos um arquivo! Por que 5 inserções? Note que o arquivo tem 5 linhas. Não, não é coincidência, o git faz o controle **linha a linha** dos arquivos (por isso eu quis fazer um texto puro, não um .doc ou qualquer outra formatação)
- "create mode 100644 readme.txt" aqui indica as permissões do arquivo criado

Bacana, acabamos de nos comprometer com mudanças feitas no repositório.

Mais pra frente, com mais prática, veremos coisas bem legais de se fazer quando definimos a mensagem de um _commit_ (spoiller: podemos fechar _issues_ e mencionar outros membros), além de entender uns padrões de [como escrever um bom _commit_](https://gist.github.com/lucasmezencio/3835117)

Bom, o que eu chamei de _ciclo básico_ do Git é isso:

1. faz alterações (pode ser mais de uma, desde que façam sentido juntas)
2. `git add <arquivo1> <arquivo2> ...` para adicionar à área de registro
3. `git commit -m "<mensagem>"` para se comprometer e deixar uma mensagem do que foi feito

Para finalizar essa seção de "básico", há mais um comando! À medida em que o desenvolvimento avança, os _commits_ vão contando a história. Para acessar o registro de _commits_, faça:

`git log`

No meu caso aqui, o obtive:

```
commit 58b365c1daf5627fca35415b3c7b0951f2428317 (HEAD -> master)
Author: João Daniel <jotaf.daniel@gmail.com>
Date:   Sat Dec 9 01:17:39 2017 -0200

    cria arquivo readme
```

Esse é o bloco relativo a um _commit_ (porque só fizemos um mesmo até agora), mas note as nossas informações:
- o hash identificador do commit
- mais informações sobre a _branch_ (`HEAD -> master`) e sobre o HEAD (mais pra frente)
- o autor e o _timestamp_ do _commit_
- e a nossa mensagem de "lembrete"


### 3) Ciclo Básico X - extendido

Ainda dentro do ciclo de ações mais frequentemente usadas do Git, há algumas coisas que já fogem do escopo de ações básicas, por serem de fato mais elaboradas e exigirem um cuidado maior. Elas são

- abertura e junção de ramos
- comunicação com repositório remoto


#### 3.1) _Branching_

O desenvolvimento quase nunca é sequencial, muitas vezes precisamos ter linhas paralelas de desenvolvimento (isso inclusive é uma boa prática). O Git suporta essa necessidade com a implementação das _branches_ - ramos de trabalho.

Já introduzindo a boa prática, suponha a situação: estamos numa situação estável do nosso  diretório de trabalho e queremos adicionar mais coisas, mas não queremos perder a condição de estabilidade do diretório. Solução: abrimos um ramo novo de desenvolvimento, um ambiente isolado para que trabalhemos a nova funcionalidade até a estabilizar.

Mais concretamente, vamos criar um novo parágrafo no nosso arquivo "readme.txt". Então, devemos abrir uma _branch_ nova para trabalhar.

`git branch novo_paragrafo`

Esse comando diz ao git pra abrir essa ramificação. Rode e note que não há qualquer tipo de retorno - parece que não aconteceu nada. No entanto, o git acabou de criar um novo ramo. Veja:

```
$ git branch

* master
  novo_paragrafo
```

> a indicação `$` significa apenas que o comando está sendo rodado sem privilégios, o comando em si é apenas `git branch` - optei por colocar tudo em um só bloco, assim temos o comando e sua saída juntos

Olha lá, ele criou a nossa _branch_ "novo_paragrafo"! Mas note que há um asterísco ao lado da _master_. Isso indica que "estamos" na _master_. Vamos mudar para a "novo_paragrafo":

```
$ git checkout novo_paragrafo

Switched to branch 'novo_paragrafo'
```

Agora sim estamos na nova _branch_ e podemos começar a fazer nossas alterações de maneira segura. Vamos adicionar este parágrafo ao "readme.txt":

```
Cada diretório de trabalho do Git é um repositório com um histórico completo
e habilidade total de acompanhamento das revisões, não dependente de acesso
a uma rede ou a um servidor central.
```

Essa foi nossa alteração. Vamos ver se o Git notou, então adicioná-la à área de registro e nos comprometer que é isso que queremos:

> note: utilizaremos o ciclo básico!

```
$ git status
On branch novo_paragrafo
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   readme.txt

no changes added to commit (use "git add" and/or "git commit -a")

$ git add readme.txt
$ git commit -m "adiciona segundo parágrafo"
[novo_paragrafo 0352dff] adiciona segundo parágrafo
 1 file changed, 4 insertions(+)
```

Maravilha! Vamos ver como está o nosso histórico:

```
$ git log

commit 0352dffe38e81fff60caa57aa1e1ada403610349 (HEAD -> novo_paragrafo)
Author: João Daniel <jotaf.daniel@gmail.com>
Date:   Sat Dec 9 10:52:01 2017 -0200

    adiciona segundo parágrafo

commit 58b365c1daf5627fca35415b3c7b0951f2428317 (master)
Author: João Daniel <jotaf.daniel@gmail.com>
Date:   Sat Dec 9 01:17:39 2017 -0200

    cria arquivo readme
```

Repare que aquilo que tínhamos feito anteriormente ainda está lá, como _commit_ mais antigo, afinal nós partimos daquele estado de _master_ para a nova _branch_.

Legal! Agora, vamos voltar ao nosso ambiente estável (sempre será a _master branch_) e ver como estão as coisas por lá:

`git checkout master`

Rode uma listagem de diretório (`ls`) e veja que nosso "readme" está aqui. E então, rode o histórico do git: `git log`. Cadê nosso último _commit_?!! De fato, neste contexto ele não existe. E não estranhe, era para acontecer isso mesmo! _Commits_ são sempre atrelados a uma _branch_. Quando _commit_amos nosso segundo parágrafo, estávamos na "novo_paragrafo", então é lá que está o _commit_.

Agora que chegamos a uma estabilidade com nosso segundo parágrafo, queremos trazê-lo para o ramo onde ficam nossas versões estáveis. O git faz isso com o conceito de fusão. Queremos fundir a _master_ com a "novo_paragrafo". Para isso, garanta que você está na _master branch_ (`git branch` e veja se o asterísco está ao lado dela - se não estiver, dê _checkout_ pra ela `git checkout master`) e vamos pedir para realizar a fusão:

```
$ git merge novo_paragrafo
Updating 58b365c..0352dff
Fast-forward
 readme.txt | 4 ++++
 1 file changed, 4 insertions(+)
```

Eis a mensagem que o Git dá quando há sucesso (eventualmente haverá conflitos, mas tentaremos evitar isso com algo que trataremos daqui a pouco). Agora nossa _master_ foi fundida à "novo_paragrafo". Rode um `git log` (lembre que estamos na _master_) e veja o que há de diferente. Pois é, agora o _commit_ do segundo parágrafo está na _master_.

Nosso trabalho com a _branch_ "novo_paragrafo" está terminado, certo? Agora podemos fazer uma das duas coisas com essa _branch_:

1. deletá-la do nosso repositório local
Perceba que não há problemas nisso, porque já fundimos ela à _master_, não perderemos nenhum conteúdo
2. renomeá-la com algum identificador de encerrada
Assim, quando rodarmos um `git branch` no futuro, todas as _branhces_ que já foram encerradas estarão com essa anotação e nossa visão será facilitada

Estando na _master_, podemos fazer as coisas ditas acima com os comandos:
1. `git branch -D novo_paragrafo` para deletar
2. `git branch -m novo_paragrafo closed_novo_paragrafo` para renomear (note o identificador "_closed\__" precedendo o novo nome)

Para seguir, eu optei por renomear, mas você pode fazer qualquer uma das duas coisas.

Como eu citei agora há pouco, há a possibilidade de alguma alteração na _branch_ nova conflitar com outras coisas desenvolvidas e já fundidas à _master_. Ok, por enquanto isso pode parecer estranho, eu concordo, mas considere o desenvolvimento em equipe, no qual várias pessoas estão mexendo no projeto. Agora você vê reais chances de conflitos.

Tem como garantir que eles não aconteçam? Eu acho que não, mas dá pra evitar muito. E a ideia é sempre a mesma: faça as mudanças na _branch_ paralela à master até que esteja tudo estável, **inclusive** a fusão. Eu vou apresentar o conceito de _rebase_.

Antes, vamos ver a situação prática. Nossa _master_ agora tem um "readme.txt" com dois parágrafos e mais nenhum arquivo. O que faremos agora são duas coisas **independentes**:
- adicionar um terceiro parágrafo
- criar um novo arquivo

Por enquanto, a situação que temos é a seguinte:

![rebase_init](/uploads/8b76421d6c84172004a2812e82da752d/rebase_init.png)

> Não estranhe as setas "pra trás", os _commits_ do Git sempre apontam para o _commit_ anterior a ele

Cada desenvolvimento em sua _branch_, então, criemos as duas novas:

```
$ git branch terceiro
$ git branch gitignore_file
$ git branch
closed_novo_paragrafo
  gitignore_file
* master
  terceiro
```

Vamos primeiro pra _branch_ "gitignore_file" com `git checkout gitignore_file`. Aqui, vamos criar um arquivo chamado ".gitignore" - ele diz para o Git quais arquivos nós não queremos que ele cuide, que não tem necessidade de ir pro repositório. Dentro do arquivo, coloque "*.swp" (se você usa Vim, sabe que esses _swap files_ salvam muito, mas nós não precisamos que o git os versione).

Novamente, ciclo básico! Vamos _commit_ar com a mensagem "cria gitignore - adiciona swap files ao ignore". Por enquanto, vamos deixar isso aí mesmo, sem _merge_, nem nada.

Agora, vamos escrever nosso terceiro parágrafo. Vamos à _branch_ `git checkout terceiro`, abrimos o arquivo "readme.txt" e escrevemos:

```
O Git é um software livre, distribuído sob os termos da versão 2 da GNU
General Public License. Sua manutenção é atualmente supervisionada por
Junio Hamano.
```

Ciclo básico, _commit_ com a mensagem "adiciona terceiro parágrafo". Também nada de _merge_ por enquanto. Dê um `checkout`para a _master_ `git checkout master`.


Qual a situação atual do repo?

![rebase_three_branches](/uploads/21c87d66aca611b8bff1702b2c7fc420/rebase_three_branches.png)

> Nota: vá às três _branches_ e veja o _log_ delas, ele coincide com a trajetória dos _commits_ na imagem, se seguirmos as setas


Vamos começar as fusões. Primeiro faremos de "gitignore_file" com a _master_. Podemos seguir o mesmo esquema da migração que já fizemos: estando na _master_, funda a "gitignore_file" com:

```
$ git merge gitignore_file
Updating 0352dff..f740bf7
Fast-forward
 .gitignore | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 .gitignore
```

Sucesso! Podemos renomear (ou deletar) a _branch_. Ficamos assim:

![rebase_first_merge](/uploads/4ad0c27ce1d8958ade0df407d863042a/rebase_first_merge.png)

Agora, para fundirmos "terceiro" com _master_, teremos que ter um cuidado. A boa prática diz que devemos garantir a estabilidade na _branch_ antes de fundir, então vamos lá na "terceiro" com `git checkout terceiro`. O que fazer agora? Lá em cima eu falei que apresentaria o conceito de _rebase_ e é agora. _Rebase_ é redefinir a base de uma _branch_. O _commit_ na "terceiro" aponta lá para o _commit_ "adiciona segundo parágrafo", mas este não é mais o último _commit_ da _master_. Vamos fazer o _rebase_ da "terceiro" e ver o que acontece. Para isso, garanta que estamos na "terceiro" e então:

```
$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: adiciona terceiro parágrafo
```

Leia com calma a mensagem do Git e, então, rode um `git log`. O _commit_ fundido do gitignore agora está também na _branch_ "terceiro"! Então, nossa situação acabou de mudar para isso:

![rebase_rebased](/uploads/b9dcd5c4b03429a08a10cff53f0df33f/rebase_rebased.png)

Agora podemos fazer a fusão da _master_ com a "terceiro" com tranquilidade, afinal conseguimos deixar estável a _branch_ "terceiro".

```
$ git checkout master
Switched to branch 'master'

$ git merge terceiro
Updating f740bf7..6c91a59
Fast-forward
 readme.txt | 4 ++++
 1 file changed, 4 insertions(+)

$ git branch -m terceiro closed_terceiro
```

> se você tiver optado por deletar a _branch_, então seu último comando deve ser trocado por `git branch -D terceiro`

Finalizamos com a seguinte situação:

![rebase_final](/uploads/d75449c809a3eae578908cf06a97cf43/rebase_final.png)

Vamos continuar aplicando o _rebase_? Agora pra valer mesmo! Vamos causar um conflito! O que faremos serão duas coisas **independentes** porém em cima **do mesmo arquivo**. Uma será criar um link para a página da wikipédia de onde tiramos nossos textos, outra será fazer um rodapé indicando o fim do arquivo, nosso nome e o ano desse documento. Duas _branches_: "rodape" e "citacao":

```
$ git branch citacao
$ git checkout -b rodape
Switched to a new branch 'rodape'
```

> Repare que, desta vez, eu criei uma _branch_ do jeito tradicional, mas a outra foi criada através de outro comando o `git checkout -b <branch>`: com ele, além de criar a _branch_, ele também já faz `checkout` (vai) pra ela! Bastante prático.

Muito bem, rode o `git branch` e verifique que você está na na "rodape". Abra o arquivo "readme.txt" e coloque (com o seu nome):

```
----------
João Daniel - 2017
----------
```

O que faremos agora? Ciclo básico! _Commit_ com a mensagem "adiciona rodapé" e deixa tudo por aí mesmo, sem _merge_.

Bora pra outra _branch_ `git checkout citacao`! Aqui o nosso rodapé não está presente. Sem surpresas, certo? Adicione o link com o texto:

```
Texto retirado de
https://pt.wikipedia.org/wiki/Git
```

_Commite_ com "Faz referência à wikipedia" e vamos fundir à _master_.

```
$ git checkout master
Switched to branch 'master'

$ git merge citacao
Updating 6c91a59..713d7be
Fast-forward
 readme.txt | 4 ++++
 1 file changed, 4 insertions(+)
```

Tranquilidade! Agora, a fusão da outra _branch_...

```
$ git checkout rodape
Switched to branch 'rodape'

$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: adiciona rodapé
Using index info to reconstruct a base tree...
M	readme.txt
Falling back to patching base and 3-way merge...
Auto-merging readme.txt
CONFLICT (content): Merge conflict in readme.txt
error: Failed to merge in the changes.
Patch failed at 0001 adiciona rodapé
The copy of the patch that failed is found in: .git/rebase-apply/patch

Resolve all conflicts manually, mark them as resolved with
"git add/rm <conflicted_files>", then run "git rebase --continue".
You can instead skip this commit: run "git rebase --skip".
To abort and get back to the state before "git rebase", run "git rebase --abort".
```

Eu apresento o **CONFLITO**! Mas calma, não se assuste! A mensagem dele é exatamente o que a gente precisa saber para resolver o conflito e seguir sem problemas! Tá vendo porquê é bom estabilizar _até o merge na branch_ antes de fundir na _master_?

E o que essa mensagem diz? `CONFLICT (content): Merge conflict in readme.txt` é uma linha bastanta importante, ela nos diz quais arquivos conflitaram. Seguindo, `Resolve all conflicts manually` então bora abrir o "readme.txt" e ver o estrago:

```
<<<<<<< HEAD
Texto retirado de
https://pt.wikipedia.org/wiki/Git

=======
----------
João Daniel - 2017
----------
>>>>>>> adiciona rodapé
```

Olha com bastante calma esse trecho que eu expus. Reconhece que são exatamente as partes que criamos em cada uma das _branches_? O Git separou os blocos conflitantes com os seguintes delimitadores:

```
<<<<<<< HEAD

=======

>>>>>>> adiciona rodapé
```

Entre `<<<<<<<` e `=======` é o bloco _local_ indicado pelo `HEAD`: é onde estamos. Como estamos na _branch_ "citacao", esse primeiro bloco contém exatamente o que adicionamos fazendo referência à Wikipedia.

Entre `=======` e `>>>>>>>` é o bloco _externo_ indicado pela mensagem de _commit_ `adiciona rodapé`. Esse bloco é o que estava na _master_ quando fizemos o _rebase_, contém exatamente o que tinha sido adicionado lá na "rodape" e foi fundido na _master_ agora há pouco.

Bora resolver esse conflito? Eu quero que fiquem as duas coisas, preferencialmente a citação antes do rodapé. Então eu limpo as marcas do Git e deixo do jeito que me interessa:

```
Texto retirado de
https://pt.wikipedia.org/wiki/Git

----------
João Daniel - 2017
----------
```

Salvo o arquivo dessa forma. E o que mais aquela mensagem de erro nos dizia? `Resolve all conflicts manually, mark them as resolved with "git add/rm <conflicted_files>", then run "git rebase --continue"`. Certo, já resolvemos, então precisamos dizer ao git que tá ok  e podemos seguir com o _rebase_:

```
$ git add readme.txt
$ git rebase --continue
Applying: adiciona rodapé
```

Será que tá tudo ok? O `git status` diz que parece que sim. Para garantir, rode um histórico e veja se a sequência de _commits_ está como deveria (esperamos que o _commit_ da citação esteja depois do terceiro parágrafo e antes do rodapé).

```
$ git log
commit dc5da1036c49e01ec5fbbf22103b9c908c686e8a (HEAD -> rodape)
Author: João Daniel <jotaf.daniel@gmail.com>
Date:   Sat Dec 9 14:31:02 2017 -0200

    adiciona rodapé

commit 713d7be88b4feba160edb5f699631bedfc49613a (master, citacao)
Author: João Daniel <jotaf.daniel@gmail.com>
Date:   Sat Dec 9 14:34:17 2017 -0200

    Faz referência à wikipedia

commit 6c91a59aba4cb45490e6bb7732ff49c1b26fea5b (closed_terceiro)
Author: João Daniel <jotaf.daniel@gmail.com>
Date:   Sat Dec 9 11:39:09 2017 -0200

    adiciona terceiro parágrafo
```

Tá certo! Então conseguimos fazer o _rebase_ dessa _branch_ "rodape"! Agora podemos fazer o _merge_ com a _master_ tranquilamente:

```
$ git checkout master
Switched to branch 'master'

$ git merge rodape
Updating 713d7be..dc5da10
Fast-forward
 readme.txt | 3 +++
 1 file changed, 3 insertions(+)

$ git branch -m rodape closed_rodape
$ git branch -m citacao closed_citacao
```

Assim eu acabei de mostrar que conflitos acontecem, sim, mas que estão longe de ser difíceis de resolver com o Git (talvez você tenha que achar o responsável por aquela alteração para ver o que fazer, mas chegando num acordo, o problema é facilmente resolvível).


#### 3.2) Repositório Remoto

Agora sim vamos tratar de como equipes conseguem desenvolver um projeto único. Até agora estávamos mexendo apenas no repositório local, o que nós criamos na nossa máquina e que está disponível apenas para nós mesmos. Porém o Git oferece suporte a repositórios remotos, isso que possibilidade a integração de ferramentas como o [GitLab](/tutorial-de-gitlab).

Sendo bastante objetivo, claro e preciso na definição, um repositório remoto nada mais é do que um conjunto de _branches_ que não estão no repositório local. Nós já lidamos com várias _branches_ aqui, não lidamos? Pois bem, com repositório remoto é igual. Acabou!

Na realidade, é praticamente idêntico, sim, mas há uns comandos um pouco diferentes. Por exemplo, não se faz _merge_ diretamente com um _branch_ do remoto. O que se faz é um _pull_ ou um _push_. Mas vamos com calma.

Se a gente já precisava ter um cuidado todo especial com a nossa _master_ local, com a _master_ remota precisamos ainda mais! A local somente a gente acessa, mas a remota é a base de confiança de todo o projeto, todo mundo que desenvolve **depende** da _master_ remota. Melhor não a estragar.

Como eu faço para atribuir um repositório remoto ao meu local? E se o meu local já estiver atrelado a um remoto? Vamos verificar tudo isso. Primeiro, vamos ver se já temos alguma indicação remota:

`git remote`

No meu caso (e no seu também, se você tiver criado seu repositório junto comigo), este comando não disse nada. Isso porque não há nenhum repo remoto vinculado. Então vamos vincular.

Primeiro, eu preciso ter um repo remoto disponível. O mais prático é criar um em algum site, como este aqui o GitLab. Faça uma conta, entre e procure por "New project". Coloque o nome que desejar, informe uma descrição no formulário que lhe for exibido e salve. Se tudo der certo, você deve cair numa página como essa:

![gitlab_project_main_page](/uploads/b0eb967116b51a93e1d9c1203b51661b/gitlab_project_main_page.png)

Logo abaixo do ícone, do título e da descrição, há uma caixa de diálogo com algo que parece uma URL, certo? Ao lado esquerdo dela, selecione "SSH" e copie a URL. Ela identifica o nosso repo remoto. Vamos adicioná-la ao nosso projeto para que ele saiba pra onde levar nossos arquivos.

`git remote add origin <URL>`

Esse comando adiciona a nossa URL como repo remoto e define um rótulo pra ele, no caso `origin` (isso é uma convenção, mas você pode dar o rótulo que quiser). Agora, rode novamente `git remote` e note que há o rótulo que acabamos de criar. O Git aceitou nosso repo remoto.

Note que, como vamos utilizar o protocolo SSH, precisamos de chaves para a identificação. Vá em suas [configurações de perfil no Git Lab](https://gitlab.com/profile) e selecione SSH Keys no menu à esquerda. Leia a explicação de como gerar uma chave, caso seja necessário, e cole sua chave no espaço reservado. Desse modo, essa chave pública SSH está vinculada à sua conta, então isso já bastará para fazer a sua identificação - usuário e senha não serão necessários.

De volta ao projeto, se olharmos no GitLab, veremos que as coisas lá ainda não estão preenchidas, parece que o projeto tá vazio. E de fato está. Para enviar nosso repo local para o remoto, devemos dizer explicitamente ao Git, fazendo:

```
$ git push
fatal: The current branch master has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin master
```

Parece que rolou um erro, não é? Na realidade, não há registro de nada no repo remoto, então a primeira vez precisa ser bem explícita que, além de enviar arquivos, estamos pedindo que seja criada uma estrutura de suporte (uma _branch_ correspondente). Vamos fazer então o que a mensagem nos disse:

```
$ git push --set-upstream origin master
Counting objects: 18, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (15/15), done.
Writing objects: 100% (18/18), 2.01 KiB | 2.01 MiB/s, done.
Total 18 (delta 4), reused 0 (delta 0)
To https://gitlab.com/jotaf.daniel/git_tuto.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

Algumas mensagens importantes:
- note que é feito um _upload_ de várias coisas com compressão e tudo mais - esse é o envio do local para o remoto;
- por último: `Branch 'master' set up to track remote branch 'master' from 'origin'`. Isto é, nosso _branch_ **local** _master_ está atrelado agora ao _branch_ **remoto** origin/master.

Acabamos de enviar nossas alterações e contribuições para o repo remoto. Mas como fazemos para buscar as de lá e trazer para o local? Aqui é bem importante que tenhamos em mente que estamos lidando com _branches_ diferente novamente. Vamos trazer as alterações de _origin/master_ para _master_ (notou, ja estou diferenciando local de remoto com o _origin_). Assim como fizemos com o "readme.txt" na hora de colocar citação e rodapé, vamos ter que tomar cuidado com conflitos. Vamos deixar sempre os conflitos longe da _branch_ mais importante. No caso, entre _master_ e _origin/master_, a última é mais importante, então trazemos para o local usando o princípio do _rebase_. Aqui, a diferença vai ser no comando:

`git pull --rebase`

Não estamos lidando apenas com _branches_ locais. Precisamos dizer ao Git que agora precisamos fazer _download_ de vários arquivos de um repo remoto, por isso o `pull`.


> `git pull --rebase` e `git push` também fazem parte do Ciclo Básico


## Encerramento

Nessa página wiki, eu pude explicar alguns comandos

- `git init`
- `git status`
- `git log`
- `git add`
- `git commit`
- `git checkout`
- `git branch`
- `git rebase`
- `git merge`
- `git remote`
- `git pull`
- `git push`

Existem muitos outros comandos, muitas outras funcionalidades, mas com esse conhecimento que eu abordei, já dá pra ter bastante conforto lidando com as situações mais corriqueiras de desenvolvimento em times usando Git.

Essa foi apenas uma iniciação. Espero que você seja capaz de seguir em frente com o uso do Git!
